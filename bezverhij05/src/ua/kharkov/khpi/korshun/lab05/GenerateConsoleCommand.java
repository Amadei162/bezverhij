package ua.kharkov.khpi.korshun.lab05;

import java.util.InputMismatchException;
import java.util.Scanner;

import ua.kharkov.khpi.korshun.lab03.View;;

/**
 * Консольна команда Generate; шаблон Command
 * 
 * @author Коршун Олексій
 * @version 1.0
 */
public class GenerateConsoleCommand implements ConsoleCommand {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; 
	 * Обслуговує колекцію об'єктів {@linkplain ua.kharkov.khpi.korshun.lab03.Item}  
	 */
	private View view;

	/**
	 * Ініціалізує поле {@linkplain GenerateConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує інтерфейс {@linkplain View}  
	 */
	public GenerateConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'g';
	}

	@Override
	public String toString() {
		return "'g'enerate";
	}

	@Override
	public void execute() {
		int x = 0;
		Scanner in = new Scanner(System.in);
		System.out.println("Calculation.");
		System.out.println("Input 'x' number for calculation.");
		try {
			x = in.nextInt();
		} catch (InputMismatchException e) {
			System.out.println("Error input value: " + e);
			System.exit(0);
		}

		view.viewInit(x);
		view.viewShow();

	}

}
