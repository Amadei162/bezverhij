package ua.kharkov.khpi.korshun.lab05;

import java.util.Scanner;

import ua.kharkov.khpi.korshun.lab03.Item;
import ua.kharkov.khpi.korshun.lab03.View;
import ua.kharkov.khpi.korshun.lab03.ViewResult;

/**
 * Консольна команда Delete item; шаблон Command
 * 
 * @author Коршун Олексій
 * @version 1.0
 */
public class DeleteItemConsoleCommand implements ConsoleCommand {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; 
	 * Обслуговує колекцію об'єктів {@linkplain ua.kharkov.khpi.korshun.lab03.Item}  
	 */
	private View view;

	/**
	 * Повертає поле {@linkplain DeleteItemConsoleCommand # view}
	 * 
	 * @Return значення {@linkplain DeleteItemConsoleCommand # view}  
	 */
	public View getView() {
		return view;
	}

	/**
	 * Встановлює поле {@linkplain DeleteItemConsoleCommand # view}
	 * 
	 * @Param view значення для {@linkplain DeleteItemConsoleCommand # view}  
	 * @Return нове значення {@linkplain DeleteItemConsoleCommand # view}  
	 */
	public View setView(View view) {
		return this.view = view;
	}

	/**
	 * Ініціалізує поле {@linkplain DeleteItemConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує інтерфейс {@linkplain View}  
	 */
	public DeleteItemConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'd';
	}

	@Override
	public String toString() {
		return "'d'elete";
	}

	@Override
	public void execute() {
		int index = 0;
		int i = 0;
		Scanner in = new Scanner(System.in);
		System.out.println("Input index of element.");
		try {
			index = in.nextInt();
			if (index > ((ViewResult) view).getItems().size())
				throw (new IndexOutOfBoundsException());

		} catch (IndexOutOfBoundsException e) {
			System.out.println("Error input value: Index is out of range!");
			System.exit(0);
		}

		((ViewResult) view).getItems().remove(index);
		System.out.println("Element was deleted.");

	}
}
