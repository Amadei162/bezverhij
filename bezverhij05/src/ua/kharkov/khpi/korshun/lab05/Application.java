package ua.kharkov.khpi.korshun.lab05;

import ua.kharkov.khpi.korshun.lab03.View;
import ua.kharkov.khpi.korshun.lab04.ViewableTable;

/**
 * Формує і відображаєменю; реалізує шаблон Singleton
 * 
 * @Author Коршун Олексій
 * @Version 1.0  
 */
public class Application {

	/**
	 * Посилання на екземпляр класу Application; шаблон Singleton
	 * 
	 * @See Application  
	 */
	private static Application instance = new Application();

	/**
	 * Закритий конструктор; шаблон Singleton
	 * 
	 * @See Application  
	 */
	private Application() {

	}

	/**
	 * Повертає посилання на екземпляр класу Application; Шаблон Singleton
	 * 
	 * @See Application  
	 */
	public static Application getInstance() {
		return instance;
	}

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; Обслуговує колекцію
	 * об'єктів {@linkplain ua.kharkov.khpi.korshun.lab03.Item};  
	 * Ініціалізується за допомогою Factory Method  
	 */
	private View view = new ViewableTable().getView();

	/**
	 * Об'єкт класу {@linkplain Menu};  * Макрокоманда (шаблон Command)  
	 */
	private Menu menu = new Menu();

	/**
	 * Обробка команд користувача
	 * 
	 * @See Application
	 */
	public void run() {
		menu.add(new ViewConsoleCommand(view));
		menu.add(new GenerateConsoleCommand(view));
		menu.add(new FindConsoleCommand(view));
		menu.add(new DeleteItemConsoleCommand(view));
		menu.add(new SaveConsoleCommand(view));
		menu.add(new RestoreConsoleCommand(view));
		menu.execute();

	}
}
