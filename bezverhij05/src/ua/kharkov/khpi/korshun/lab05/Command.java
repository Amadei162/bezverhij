package ua.kharkov.khpi.korshun.lab05;

/**
 * Інтерфейс команди Або завдання; Шаблони: Command, Worker Thread
 * 
 * @Author Коршун Олексій
 * @Version 1.0  
 */
public interface Command {

	/** Виконання команди; шаблони: Command, Worker Thread */
	public void execute();
}
