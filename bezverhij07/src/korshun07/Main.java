package korshun07;

import ua.kharkov.khpi.korshun.lab05.ConsoleCommand;
import ua.kharkov.khpi.korshun.lab05.Menu;

/**
 * Реалізує діалог з користувачем; містить статичний метод main ()  
 * 
 * @Author Коршун Олексій   * @Version 6.0   * @See Main # main  
 */
public class Main {
	/**
	 * Консольна команда; використовується при створенні анонімних 13
	 * примірників Команд призначеного для користувача інтерфейсу; шаблон
	 * Command
	 *
	 * @Author Коршун Олексій
	 * @See ConsoleCommand
	 */
	abstract class ConsoleCmd implements ConsoleCommand {
		/** Колекція об'єктів {@linkplain Items} */
		protected Items items;
		/** Зображаєма назва команди */
		private String name;
		/** Символ гарячої клавіші команди */
		private char key;

		/**
		 * Ініціалізує поля консольної команди
		 *
		 * @Param items {@Linkplain ConsoleCmd # items}
		 * @Param name {@Linkplain ConsoleCmd # name}
		 * @Param key {@Linkplain ConsoleCmd # key}
		 */
		ConsoleCmd(Items items, String name, char key) {
			this.items = items;
			this.name = name;
			this.key = key;
		}

		@Override
		public char getKey() {
			return key;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	/**
	 * Встановлює зв'язок спостерігачів з спостерігаються об'єктами; реалізує
	 * Діалог з користувачем
	 */
	public void run() {
		Items items = new Items();
		ItemsGenerator generator = new ItemsGenerator();
		ItemsSorter sorter = new ItemsSorter();
		items.addObserver(generator);
		items.addObserver(sorter);
		Menu menu = new Menu();
		menu.add(new ConsoleCmd(items, "'v'iew", 'v') {
			@Override
			public void execute() {
				System.out.println(items.getItems());
			}
		});
		menu.add(new ConsoleCmd(items, "'a'dd", 'a') {
			@Override
			public void execute() {
				items.add("");
			}
		});
		menu.add(new ConsoleCmd(items, "'d'el", 'd') {
			@Override
			public void execute() {
				items.del((int) Math.round(Math.random() * (items.getItems().size() - 1)));
			}
		});
		menu.execute();
	}

	/**
	 * Виконується при запуску програми
	 *
	 * @Param args Параметри запуску програми
	 */
	public static void main(String[] args) {
		new Main().run();
	}
}
