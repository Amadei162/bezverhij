package korshun07;

/**
 * спостерігач; визначає методи обробки подій; використовує Event; шаблон
 * Observer
 * 
 * @author Коршун Олексій
 * @see AnnotatedObserver
 * @see Event
 */
public class ItemsGenerator extends AnnotatedObserver {
	/**
	 * Оброблювач події {@linkplain Items # ITEMS_EMPTY}; сповіщає
	 * спостерігачів; Шаблон Observer
	 *
	 * @Param observable Спостережуваний об'єкт класу {@linkplain Items}
	 * @See Observable
	 */
	@Event(Items.ITEMS_EMPTY)
	public void itemsEmpty(Items observable) {
		for (Item item : observable) {
			if (item.getData().isEmpty()) {
				int len = (int) (Math.random() * 10) + 1;
				String data = "";
				for (int n = 1; n <= len; n++) {
					data += (char) ((int) (Math.random() * 26) + 'A');
				}
				item.setData(data);

			}
		}
		observable.call(Items.ITEMS_CHANGED);
	}
}
