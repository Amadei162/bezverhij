package korshun08;

import ua.kharkov.khpi.korshun.lab03.View;
import ua.kharkov.khpi.korshun.lab03.Viewable;

/** ConcreteCreator (Шаблон проектування Factory Method) <br>
 * Оголошує метод, "Фабрика", який створює об'єкти
 *
 * @Author Коршун Олексій
 * @Version 1.0
 * @See ViewableResult
 * @See ViewableWindow # getView ()
 */
public class ViewableWindow implements Viewable {
	@Override
	public View getView() {
		return new ViewWindow();
	}
}
