package korshun08;

import ua.kharkov.khpi.korshun.lab03.View;
import ua.kharkov.khpi.korshun.lab04.ViewableTable;

/**
* Обчислення и відображенням результату. Містіть реалізацію статичного методу
* main().
*
* @Author Коршун Олексій
* @Version 7.0
* @See Main # main
*/
public class Main extends ua.kharkov.khpi.korshun.lab03.Main {
	public Main(View view) {
		super(view);
		
	}

	/**
	 * Виконується при запуску програми. 
	 * Викликає метод {@linkplain ua.kharkov.khpi.korshun.lab03.Main#menu menu()}
	 * 
	 * @param args - параметри запуску програми.
	 */
	public static void main(String[] args) {
		Main control = new Main(new ViewableWindow().getView());
		control.menu();
		System.exit(0);
	}
}
