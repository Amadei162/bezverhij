package ua.kharkov.khpi.korshun.lab04;

import ua.kharkov.khpi.korshun.lab03.View;
import ua.kharkov.khpi.korshun.lab03.ViewableResult;

/** ConcreteCreator (Шаблон проектування Factory Method) <br>
 * Оголошує метод, "Фабрика", який створює об'єкти
 *
 * @Author Коршун Олексій
 * @Version 1.0
 * @See ViewableResult
 * @See ViewableTable # getView ()
 */
public class ViewableTable extends ViewableResult {

	/** Створює відображаємий об'єкт {@linkplain ViewTable} */
	@Override
	public View getView() {
		return new ViewTable();
	}
}
