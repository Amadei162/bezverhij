package ua.kharkov.khpi.korshun.lab04;

import java.util.Scanner;

import ua.kharkov.khpi.korshun.lab03.View;

/**
 * Обчислення і видображення результатів. Містить реалізацію статичного методу
 * main().
 * 
 * @author Коршун Олексій
 * @version 3.0
 * @see Main#main
 */
public class Main extends ua.kharkov.khpi.korshun.lab03.Main {

	public Main(View view) {
		super(view);
	}

	/**
	 * Виконується при запуску програми. 
	 * Викликає метод {@linkplain ua.kharkov.khpi.korshun.lab03.Main#menu menu()}
	 * 
	 * @param args - параметри запуску програми.
	 */
	public static void main(String[] args) {
		
		Main control = new Main(new ViewableTable().getView());
		control.menu();
	}

}

