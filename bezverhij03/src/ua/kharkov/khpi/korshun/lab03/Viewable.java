package ua.kharkov.khpi.korshun.lab03;

/**
 * Creator (Шаблон проектуванняFactory Method) оголошує метод, "Фабрика" який
 * створює об'єкти
 *
 * @Author Коршун Олексій
 * @Version 1.0
 * @See Viewable#getView()  
 */
public interface Viewable {

	/** Створює об'єкт, який реалізує {@linkplain View} */
	public View getView();
}
