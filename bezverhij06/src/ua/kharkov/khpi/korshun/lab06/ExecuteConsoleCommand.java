package ua.kharkov.khpi.korshun.lab06;

import java.util.concurrent.TimeUnit;

import ua.kharkov.khpi.korshun.lab03.View;
import ua.kharkov.khpi.korshun.lab03.ViewResult;
import ua.kharkov.khpi.korshun.lab05.ConsoleCommand;

/**
 * Консольна команда Execute all threads; Шаблон Command
 * 
 * @Author Коршун Олексій
 * @Version 1.0
 */
public class ExecuteConsoleCommand implements ConsoleCommand {

	/**
	 * Об'єкт, який реалізує інтерфейс {@linkplain View}; Обслуговує колекцію
	 * об'єктів {@linkplain ua.kharkov.khpi.korshun.lab03.Item}
	 */
	private View view;

	/**
	 * Повертає поле {@linkplain ExecuteConsoleCommand # view}
	 * 
	 * @Return значення {@linkplain ExecuteConsoleCommand # view}
	 */
	public View getView() {
		return view;
	}

	/**
	 * Встановлює поле {@linkplain ExecuteConsoleCommand # view}
	 * 
	 * @Param view значення для {@linkplain ExecuteConsoleCommand # view}
	 * @Return нове значення {@linkplain ExecuteConsoleCommand # view}
	 */
	public View setView(View view) {
		return this.view = view;
	}

	/**
	 * Ініціалізує поле {@linkplain ExecuteConsoleCommand # view}
	 * 
	 * @Param view об'єкт, який реалізує {@linkplain View}
	 */
	public ExecuteConsoleCommand(View view) {
		this.view = view;
	}

	@Override
	public char getKey() {
		return 'e';
	}

	@Override
	public String toString() {
		return "'e'xecute";
	}

	@Override
	public void execute() {
		CommandQueue queue1 = new CommandQueue();
		CommandQueue queue2 = new CommandQueue();

		MaxCommand maxCommand = new MaxCommand((ViewResult) view);
		AvgCommand avgCommand = new AvgCommand((ViewResult) view);
		MinCommand minCommand = new MinCommand((ViewResult) view);
		System.out.println("Execute all threads");

		queue1.put(minCommand);
		queue2.put(maxCommand);
		queue2.put(avgCommand);
		try {
			while (avgCommand.running() || maxCommand.running() || minCommand.running()) {
				TimeUnit.MILLISECONDS.sleep(100);
			}

			queue1.shutdown();
			queue2.shutdown();
			TimeUnit.SECONDS.sleep(1);
		} catch (InterruptedException ie) {
			System.out.println(ie);
		}
		System.out.println("All done.");
	}
}
