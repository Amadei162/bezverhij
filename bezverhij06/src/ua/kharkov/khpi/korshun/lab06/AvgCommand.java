package ua.kharkov.khpi.korshun.lab06;

import ua.kharkov.khpi.korshun.lab03.Item;
import ua.kharkov.khpi.korshun.lab03.ViewResult;
import ua.kharkov.khpi.korshun.lab05.Command;

/**
 * Завдання, використовувана Оброблювачем потоку; Шаблон Worker Thread
 * 
 * @Author Коршун Олексій
 * @Version 1.0
 * @See Command
 * @See CommandQueue
 */
public class AvgCommand implements Command {

	/** Зберігає результат обробки колекції */
	private double result = 0;

	/** Прапор готовності результату */
	private int progress = 0;

	/**
	 * Обслуговує колекцію об'єктів
	 * {@linkplain ua.kharkov.khpi.korshun.lab03.Item}
	 */
	private ViewResult viewResult;

	/**
	 * Ініціалізує поле {@linkplain AvgCommand # viewResult}
	 * 
	 * @Param viewResult об'єкт класу {@linkplain ViewResult}
	 */
	public AvgCommand(ViewResult viewResult) {
		this.viewResult = viewResult;
	}

	/**
	 * Повертає поле {@linkplain AvgCommand # viewResult}
	 * 
	 * @Return значення {@linkplain AvgCommand # viewResult}
	 */
	public ViewResult getViewResult() {
		return viewResult;
	}

	/**
	 * Встановлює поле {@linkplain AvgCommand # viewResult}
	 * 
	 * @Param viewResult значення для {@linkplain AvgCommand # viewResult}
	 * @Return нове значення {@linkplain AvgCommand # viewResult}
	 */
	public void setViewResult(ViewResult viewResult) {
		this.viewResult = viewResult;
	}

	/**
	 * Повертає результат
	 * 
	 * @Return поле {@linkplain AvgCommand # result}
	 */
	public double getResult() {
		return result;
	}

	/**
	 * Перевіряє готовність результату
	 * 
	 * @Return false - якщо результат знайдений, інакше - true
	 * @See AvgCommand # result
	 */
	public boolean running() {
		return progress == 0;
	}

	@Override
	public void execute() {
		progress = 0;
		System.out.println("Find average value");
		result = 0;

		int numberCounter = viewResult.getItems().size();
		int sum = 0;

		for (Item item : viewResult.getItems()) {
			sum += item.getY();

		}
		result = sum / numberCounter;
		System.out.printf("Average result: %f\n", result);
		progress = 1;

	}
}
